import { LightningElement, wire } from 'lwc';
import fetchAccDetails from '@salesforce/apex/ApexContForAccDetailFetch.fetchAccDetails';


export default class TaskAccDetailFetch extends LightningElement 
{
    @wire(fetchAccDetails)
    test1;

    get detailsRecived()
    {
        return this.test1 ? true : false;
    }
}