import { api, LightningElement } from 'lwc';

export default class ChildComponent extends LightningElement {

@api objContact;
get appliedClass()
{
    return this.objContact.isActive ? "slds-text-color_success"  : "slds-text-color_error";
}

inputHandler(event)
{
    this.updatedValue = event.target.value;
}
}