import { LightningElement, track, wire } from 'lwc';
import getAccountDetails from '@salesforce/apex/ApexCtrlAccDetailByName.getAccountDetails';

export default class TaskAccDetailsByName extends LightningElement {

    @track test1;
      
    get detailsRecived()
    {
        return this.test1 ? true : false;
    }
    @track searchkey;
    @wire(getAccountDetails, { searchkey : "$searchkey"}) 
    
    accountHandler({data, error})
    {
        if(data)
        {
            this.test1 = data;
        }
        if(error)
        {
            console.error(error);
        }
    }                  
    //test deploy
    searchkeyHandler(event)
    {
        this.searchkey = event.target.value;
    }
}