public with sharing class ApexCtrlAccDetailByName {
    @AuraEnabled(cacheable = true)
    public static List<Account> getAccountDetails(string searchkey)
    {
        try 
        {    
        return [SELECT Id, Name, Phone, CreatedById, LastModifiedById FROM Account WHERE Name =: searchkey]; 
        } 
        catch (Exception e) 
        {
            throw new AuraHandledException(e.getMessage());
        }
    }
}