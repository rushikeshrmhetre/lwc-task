public class GetCountOfStates 
{
	public void GetCount()
    {
        Map<String,Integer> mapState_Count = new Map<String,Integer>(); // used map for store <key,value>=<MailingState,No of accounts>
        List<Mailing_State__c> lstMs_upsert= new List<Mailing_State__c>();  //used list to upsert mailing state.
		Set<String> setExisting_states= new Set<String>();   // created set to store unique mailing state
        
		for(Account objAcc: [SELECT MailingState__c FROM Account WHERE MailingState__c!=NULL])// iterating account object to store value in map
		{
			if(mapState_Count.containsKey(objAcc.MailingState__c)==False)
			{
				mapState_Count.put(objAcc.MailingState__c,1);  // if map doesn't contains key value
			}
			else
			{
				mapState_Count.put(objAcc.MailingState__c,mapState_Count.get(objAcc.MailingState__c)+1); // if map already contains key value, just update no of accounts 	z
			}
			
		}
		
		for(Mailing_State__c objMs : [SELECT Id,Name,No_of_Accounts__c FROM Mailing_State__c]) // iterating values from mailing state custom object
		{
			if(objMs.No_of_Accounts__c !=mapState_Count.get(objMs.Name))// checking if their any value to be update (comparing with map and no of account)
			{
				objMs.No_of_Accounts__c=mapState_Count.get(objMs.Name); // if not updated then store value and update into list
				lstMs_upsert.add(objMs);     
			}
			setExisting_states.add(objMs.Name);// update the state name in set only to store unique values of state
		}
        
		for(String strMs: mapState_Count.keySet()) // iterating key value only for comparing with set .
		{ 
			if(!setExisting_states.contains(strMs)) // here we will get the new state and need to update.
			{
                Mailing_State__c objMs= new Mailing_State__c(Name= strMs,No_of_Accounts__c=mapState_Count.get(strMs));// if we found new state then add new state with no of accounts associate;	 update the values in list
				lstMs_upsert.add(objMs);
			}
            
		}
        
		if(!lstMs_upsert.isEmpty())
			upsert lstMs_upsert;       // update the list in database
      
    }
}