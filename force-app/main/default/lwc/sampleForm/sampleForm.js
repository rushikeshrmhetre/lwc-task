import { LightningElement, track } from 'lwc';

export default class SampleForm extends LightningElement {

    // JSON
    // List = [];
    // Object = {key : "value"};
    // student = {
    //     firstName : "SLIV0002",
    //     lastName: "SLI",
    //     Age : "21",
    //     RollNumber : "12345678",
    //     "Department": "CSE",
    //     "section" :1,
    //     "phoneNumber": 9087654321,
    //     books : ['ADA', 'Data structure', 'M1', 'ML'],
    //     fullTime: true
    // }
    // ListStudents = [
    //     {
    //         firstName : "SLIV0003",
    //         lastName: "SLI",
    //         Age : "21",
    //         RollNumber : "12345678",
    //         "Department": "CSE",
    //         "section" :1,
    //         "phoneNumber": 9087654321,
    //         books : ['ADA', 'Data structure', 'M1', 'ML'],
    //         fullTime: true
    //     },

    //     this.student
    // ]
    @track formValues = {};
    // @track firstName;
    // @track lastName;
    // @track gender;
    // @track designation;
    // @track fullTime;
    // @track salary;
    // @track email;
    // @track description;
    @track submitted;

    firstNameHandler = (event) => {
        this.formValues.firstName = event.target.value;
    }
    
    lastNameHandler(event)
    {
        this.formValues.lastName = event.target.value;
    }

    genderHandler(event)
    {
        this.formValues.gender = event.target.value;
        console.log('genderHandler', this.formValues.gender );
    }

    designationHandler(event)
    {
        this.formValues.designation = event.target.value;
    }

    fullTimeHandler(event)
    {
        this.formValues.fullTime = event.target.checked;
        console.log('fullTimeHandler', this.formValues.fullTime );
    }

    salaryHandler(event)
    {
        this.formValues.salary = event.target.value;
    }

    emailHandler(event)
    {
        this.formValues.email = event.target.value;
    }

    descriptionHandler(event)
    {
        this.formValues.description = event.target.value;
    }

    submitHandler()
    {
        console.log('submitHandle');
        Object.keys(this.formValues).forEach(ele => {
            if(this.formValues[ele])
            console.log(ele)
        })
        // if(this.firstName && this.lastName && this.gender && this.fullTime && this.designation && this.salary && this.email && this.description)
        // {
        //     this.submitted = true;
        // }
        // else
        // {
        //     this.submitted = false;
        // }
    }
}