import { LightningElement, track } from 'lwc';
import createAccount from '@salesforce/apex/ApexCtrlInsertAcc.createAccount';

export default class TaskCreateAcc extends LightningElement 
{
    @track objAcc ={};
    @track accountId;

    value = "";

    get optionsType() {
        return [
            { label: 'Prospect', value: 'Prospect' },
            { label: 'Customer - Direct', value: 'Customer - Direct' },
            { label: 'Customer - Channel', value: 'Customer - Channel' },
            { label: 'Channel Partner / Reseller', value: 'Channel Partner / Reseller' },
            { label: 'Installation Partner', value: 'Installation Partner' },
            { label: 'Technology Partner', value: 'Technology Partner' },
            { label: 'Other', value: 'Other' },
        ];
    }
    get optionsOwnership() {
        return [
            { label: 'Public', value: 'Public' },
            { label: 'Private', value: 'Private' },
            { label: 'Subsidiary', value: 'Subsidiary' },
            { label: 'Other', value: 'Other' },
        ];
    }
    get optionsIndustry() {
        return [
            { label: 'Agriculture', value: 'Agriculture' },
            { label: 'Apparel', value: 'Apparel' },
            { label: 'Banking', value: 'Banking' },
            { label: 'Biotechnology', value: 'Biotechnology' },
            { label: 'Chemicals', value: 'Chemicals' },
            { label: 'Communications', value: 'Communications' },
            { label: 'Consulting', value: 'Consulting' },
            { label: 'Education', value: 'Education' },
            { label: 'Electronics', value: 'Electronics' },
            { label: 'Energy', value: 'Energy' },
            { label: 'Engineering', value: 'Engineering' },
            { label: 'Entertainment ', value: 'Entertainment' },
            { label: 'Environmental', value: 'Environmental' },
            { label: 'Finance', value: 'Finance' },
            { label: 'Food & Beverage', value: 'Food & Beverage' },
            { label: 'Government', value: 'Government' },
            { label: 'Healthcare', value: 'Healthcare' },
            { label: 'Hospitality', value: 'Hospitality' },
            { label: 'Insurance', value: 'Insurance' },
            { label: 'Machinery', value: 'Machinery' },
            { label: 'Manufacturing', value: 'Manufacturing' },
            { label: 'Media', value: 'Media' },
            { label: 'Not For Profit', value: 'Not For Profit' },
            { label: 'Recreation', value: 'Recreation' },
            { label: 'Retail', value: 'Retail' },
            { label: 'Shipping', value: 'Shipping' },
            { label: 'Technology', value: 'Technology' },
            { label: 'Telecommunications', value: 'Telecommunications' },
            { label: 'Transportation', value: 'Transportation' },
            { label: 'Utilities', value: 'Utilities' },
            { label: 'Other', value: 'Other' },        
        ];
    }


    createRecordHandler()
    {
        console.log("obj..........", this.objAcc);
        createAccount({objAcc:this.objAcc})
        .then(res => this.accountId = res)
        .catch(error => console.error(error));
    }

    inputHandler(event)
    {
        let fieldName = event.target.name;
        let val = event.target.value;
        this.objAcc[fieldName] = val;
    }
    emp = 0;
    empHandler(event)
    {
        let fieldName = event.target.name;
        this.emp = event.target.value;
        this.objAcc[fieldName] = Number(this.emp);
        console.log(fieldName ,  event.target.value);
        console.log(this.objAcc);
    }

    createAccountHandler()
    {
        this.createRecordHandler();
    }
}