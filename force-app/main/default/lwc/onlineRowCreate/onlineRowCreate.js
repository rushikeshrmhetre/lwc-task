// import { LightningElement, track } from 'lwc';
// import { NavigationMixin } from 'lightning/navigation';
// import { ShowToastEvent } from 'lightning/platformShowToastEvent';
// export default class DynamicRecordCreationRows extends NavigationMixin(LightningElement) {

//     keyIndex = 0;
//     @track itemList = [
//         {
//             id: 0
//         }
//     ];

//     addRow() {
//         ++this.keyIndex;
//         var newItem = [{ id: this.keyIndex }];
//         this.itemList = this.itemList.concat(newItem);
//     }

//     removeRow(event) {
//         if (this.itemList.length >= 2) {
//             this.itemList = this.itemList.filter(function (element) {
//                 return parseInt(element.id) !== parseInt(event.target.accessKey);
//             });
//         }
//     }

//     handleSubmit() {
//         var isVal = true;
//         this.template.querySelectorAll('lightning-input-field').forEach(element => {
//             isVal = isVal && element.reportValidity();
//         });
//         if (isVal) {
//             this.template.querySelectorAll('lightning-record-edit-form').forEach(element => {
//                 element.submit();
//             });
//             this.dispatchEvent(
//                 new ShowToastEvent({
//                     title: 'Success',
//                     message: 'Contacts successfully created',
//                     variant: 'success',
//                 }),
//             );
//             // Navigate to the Account home page
//             this[NavigationMixin.Navigate]({
//                 type: 'standard__objectPage',
//                 attributes: {
//                     objectApiName: 'Contact',
//                     actionName: 'home',
//                 },
//             });
//         } else {
//             this.dispatchEvent(
//                 new ShowToastEvent({
//                     title: 'Error creating record',
//                     message: 'Please enter all the required fields',
//                     variant: 'error',
//                 }),
//             );
//         }
//     }

// }

import {
    LightningElement,
    track,
  } from "lwc";
  
  export default class onlineRowCreate extends LightningElement {
    @track data = [];
    accountName;
    accountWebsite;
    accountRevenue;
    key = 0;
  
    connectedCallback() {
      this.data = [{
        id: 0,
        name: "",
        website: "",
        revenue: ""
      }];
    }
  
    handleChange(event) {
      this[event.target.name] = event.target.value;
    }
   
    // add row
    handleAdd() {
      this.saveRecord();
      this.data = [...this.data, {
        id: this.key + 1,
        name: "",
        website: "",
        revenue: ""
      }];
    }
  
    // save data in an array
    saveRecord() {
      if (this.data.length === 1) {
        this.data = [{
          id: 0,
          name: this.accountName,
          website: this.accountWebsite,
          revenue: this.accountRevenue
        }];
      } else {
        this.key++;
        this.data.splice(this.data.length - 1, 1);
        this.data = [
          ...this.data,
          {
            id: this.key,
            name: this.accountName,
            website: this.accountWebsite,
            revenue: this.accountRevenue
          }
        ];
      }
    }
  
    // delete row
    handleRemove(event) {
      this.data = this.data.filter(val => {
        return parseInt(val.id) !== parseInt(event.target.accessKey)
      });
    }
  
    // Saving last row data and Commiting to database
    handleSubmit() {
      this.data[this.data.length - 1].name = this.accountName;
      this.data[this.data.length - 1].website = this.accountWebsite;
      this.data[this.data.length - 1].revenue = this.accountRevenue;
  
      // TODO -- save records to database
    }
  }