public with sharing class ApexCtrlInsertAcc 
{
   @AuraEnabled
   public static string createAccount(Account objAcc)
   {
       try 
       {    
           if(String.isNotBlank(objAcc.Name))
            
            {
                // System.debug(JSON.serializePretty(objAcc));
               insert objAcc;
               return 'Account created Successfully';
            }   
            else 
            {
                return 'Please Fill Details again,Account not created';
            }
       } 
       catch (Exception e) 
       {
           throw new AuraHandledException(e.getMessage());
       }
   }
}