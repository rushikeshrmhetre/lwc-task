import { LightningElement, track } from 'lwc';

export default class ParentComponent extends LightningElement {

    @track listContact = [
        {Name: "test1", Phone: 1234123412, isActive: true},
        {Name: "test2", Phone: 1234123413, isActive: true},
        {Name: "test3", Phone: 1234123415, isActive: true},
        {Name: "test4", Phone: 1231231234, isActive: false},
        {Name: "test5", Phone: 1231231239, isActive: true}
    ];

    @track inputValue;
}

