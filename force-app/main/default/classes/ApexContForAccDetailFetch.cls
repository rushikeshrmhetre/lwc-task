public with sharing class ApexContForAccDetailFetch {
    @AuraEnabled(cacheable = true)
    public static List<Account> fetchAccDetails()
    {
        try 
        {
        return [SELECT Id, Name, Phone FROM Account]; 
        } 
        catch (Exception e) 
        {
            throw new AuraHandledException(e.getMessage());
        }
    }
}