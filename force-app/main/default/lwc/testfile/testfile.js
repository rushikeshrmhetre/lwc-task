import { LightningElement,track } from 'lwc';

export default class Testfile extends LightningElement {
    value = '';
    get options() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
    }
    keyIndex = 0;
    @track detailList = [
        {
            id: 0,
            edit: true
        }
    ];
    addRow() {
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex, edit: true }];
        this.detailList = this.detailList.concat(newItem);
        console.log("Add Row"+JSON.stringify(this.detailList));
    }
    removeRow(event) {
        if (this.detailList.length >= 1) {
            console.log(event.target.accessKey+"event.target.accesskey1111111");
            this.detailList = this.detailList.filter(function (element) {
                console.log(parseInt(element.id)+"parseelement");
                console.log("parseInt==" +parseInt(event.target.accessKey));
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
    }
    list = {
        Name : this.name,
        Gender : this.value,
        Desig : this.desig,
        // fulltime : this.fulltime,
        Salary : this.salary,
        Email : this.email,
        Desc : this.desc,
        edit : true
    }
    handleNameChange(event) {
        this.list.Name = event.target.value;
        console.log("name", this.list.Name);
        // console.log("start");
        // var row = event.target.accessKey;
        // console.log(row+"row");
        // this.detailList[row].Name = event.target.value;
        // console.log("finish");
    }
    handleDesigChange(event) {
        this.list.Desig = event.target.value;
        console.log("desig", this.list.Desig);
    }
    handleSalChange(event) {
        this.list.Salary = event.target.value;
        console.log("sal", this.list.Salary+"event.target.value"+event.target.value);
    }
    handleEmailChange(event){
        this.list.Email = event.target.value;
        console.log("email", this.list.Email);
    }
    handleDesChange(event){
        //get the index/id of the of the row
        //with the id get the object
        //then temp.Desc = event.target.value.
        this.list.Desc = event.target.value;
        console.log("descry", this.list.Desc);
    }
    handleGenderChange(event){
        this.list.Gender = event.target.value;
        console.log("gender", this.list.Gender);
    }
    saveRow(event){
        // let nameElement = event.target.name;
        // let idd = event.target.accessKey;
        // console.log("nameElement>>> "+nameElement);
        // console.log("Iddd>>>>> "+idd);
        // // console.log("this.detailList[i].id"+this.detailList[idd].id);
        // console.log("event.target.dataset.Id"+event.target.accessKey);
        // console.log("event.target.value"+event.target.value);
        // console.log("this.detailList[idd].Name"+this.detailList[idd].Name);
        // this.detailList[idd].Name = event.target.value;
        // console.log("finish save");
        ////////////////////
        // let nameElement = event.target.name;
        // for(let i=0;i<this.detailList.length;i++){
        //     console.log("this.detailList[i].id"+this.detailList[i].id);
        //     console.log("event.target.dataset.Id"+event.target.dataset.id);
        //     if(this.detailList[i].id === event.target.dataset.id)
        //         this.detailList[i].Name = event.target.value;
        // }
        //////////////////////////////////////
        // console.log("finish");
        // console.log("start save"+JSON.stringify(this.list));
        // var idd = parseInt(event.target.accessKey);
        // console.log("save id "+idd);
        // this.detailList = [this.list];
        // console.log("End save"+JSON.stringify(this.detailList));
        // var newItem = [{ id: this.keyIndex}];
        // console.log("Data of json"+JSON.stringify(list));
        ///////////
        console.log("start"+parseInt(event.target.dataset.id));
        var row = event.target.accessKey;
        for(let i=0;i<this.detailList.length;i++){
            if(this.detailList[i].id == row){
                console.log(this.detailList[i].id+"id");
                console.log(row+"row");
                console.log(JSON.stringify(this.detailList[i].Name+"name"));
                this.detailList[i].Name = this.list.Name;
                this.detailList[i].Gender = this.list.Gender;
                this.detailList[i].Desig = this.list.Desig;
                this.detailList[i].Salary = this.list.Salary;
                this.detailList[i].Email = this.list.Email;
                this.detailList[i].Desc = this.list.Desc;
                this.detailList[i].edit = false;
                console.log("this.list.Name"+this.list.Name);
            }
        }
    }
    editRow(event){
        console.log("start Edit");
        var row = event.target.accessKey;
        for(let i=0;i<this.detailList.length;i++){
            if(this.detailList[i].id == row){
                this.detailList[i].edit = true;
            }
        }
        console.log("finish Edit");
    }
}